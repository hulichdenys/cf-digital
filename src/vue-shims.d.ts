declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'gridsome';
declare module 'rellax';

declare module '*.svg' {
  const content: any;
  export default content;
}
