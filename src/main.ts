/**
 * This is the main.ts file. Import global CSS and scripts here.
 * Learn more: gridsome.org/docs/client-api
 */

/**
 * Types imports
 */
import { ClientApiConstructor } from './types';

/**
 * Global scss imports
 */
import '~/assets/scss/main.scss';

/**
 * Layout imports
 */
import DefaultLayout from '~/layouts/Default.vue';

/**
 *
 * Bootstrap-vue components import
 */
import {
  // CarouselPlugin,
  // AspectPlugin,
  // ImagePlugin,
  // TabsPlugin,
  LayoutPlugin,
} from 'bootstrap-vue';

/**
 *
 * Vue-rellax import
 */

/**
 * Client API contructor
 */
const client: ClientApiConstructor = (Vue) => {
  Vue.component('Layout', DefaultLayout);

  /**
   * Bootstrap components
   */
  Vue.use(LayoutPlugin);
  // Vue.use(CarouselPlugin);
  // Vue.use(AspectPlugin);
  // Vue.use(ImagePlugin);
  // Vue.use(TabsPlugin);
};

export default client;
