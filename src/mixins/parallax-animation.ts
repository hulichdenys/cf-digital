import Vue, { VueConstructor } from 'vue';

interface ComponentInstance {
  $refs: {
    main: HTMLElement;
    footer: HTMLElement;
  };
}

export default (Vue as VueConstructor<Vue & ComponentInstance>).extend({
  mounted() {
    this.initializeParallaxFooter();
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.updateParallaxFooter);
    window.removeEventListener('scroll', this.updateParallaxFooter);
  },
  methods: {
    initializeParallaxFooter() {
      this.$refs.footer.style.left = '0';
      this.$refs.footer.style.right = '0';
      this.$refs.footer.style.zIndex = '-1';

      this.updateParallaxFooter();

      window.addEventListener('resize', this.updateParallaxFooter);
      window.addEventListener('scroll', this.updateParallaxFooter);
    },
    updateParallaxFooter() {
      const main: HTMLElement = this.$refs.main;
      const footer: HTMLElement = this.$refs.footer;
      const body: any = document.querySelector('body');

      if (this.isViewportSmallerThanFooter()) {
        footer.style.bottom = '';
        footer.style.top = '0';
      } else {
        footer.style.top = '';
        footer.style.bottom = '0';
      }
      if (window.scrollY > this.getBottomY(main)) {
        footer.style.position = 'static';
        body.style.marginBottom = '0px';
      } else {
        body.style.marginBottom = footer.offsetHeight + 'px';
        footer.style.position = 'fixed';
      }
    },
    isViewportSmallerThanFooter(): boolean {
      return window.innerHeight < this.$refs.footer.offsetHeight;
    },
    getBottomY($element: HTMLElement): number {
      return $element.offsetTop + $element.offsetHeight;
    },
  },
});
