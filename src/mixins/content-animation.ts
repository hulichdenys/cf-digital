import Vue, { VueConstructor } from 'vue';

interface ComponentInstance {
  $refs: {
    container: HTMLElement;
    content: HTMLElement;
  };
  //TODO: intersection observer types
  observer: any;
}

export default (Vue as VueConstructor<Vue & ComponentInstance>).extend({
  data() {
    return {
      observer: null,
    };
  },
  mounted() {
    this.createScene();
  },
  beforeDestroy() {
    this.destroyScene();
  },
  methods: {
    /**
     * Toggle animation class
     * @param status
     */
    toggleAnimationClass(status: boolean) {
      const container = this.$refs.container;
      const content = this.$refs.content;

      if (status) {
        container.classList.add('active');
        content.classList.add('active');
      } else {
        container.classList.remove('active');
        content.classList.remove('active');
      }
    },
    /**
     * Create animation scene
     */
    createScene() {
      const options = { threshold: 0.01 };

      this.observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.intersectionRatio > 0 && entry.isIntersecting) {
            this.toggleAnimationClass(true);
          } else {
            this.toggleAnimationClass(false);
          }
        });
      }, options);

      this.observer.observe(this.$refs.container);
    },
    /**
     * Destroy animation scene
     */
    destroyScene() {
      this.observer.unobserve(this.$refs.content);
    },
  },
});
