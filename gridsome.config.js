module.exports = {
  siteName: 'Gridsome',
  icon: 'src/assets/favicon.png',
  plugins: [
    /**
     * Gridsome plugin typescript
     * Doc: https://gridsome.org/plugins/gridsome-plugin-typescript
     */
    { use: 'gridsome-plugin-typescript' },
    /**
     * Gridsome resources loader plugin
     * Doc: https://gridsome.org/plugins/gridsome-plugin-sass-resources-loader
     */
    {
      use: 'gridsome-plugin-sass-resources-loader',
      options: {
        resources: [
          '~/assets/scss/abstracts/_mixins.scss',
          '~/assets/scss/abstracts/_variables.scss',
          'bootstrap/scss/_functions.scss',
          'bootstrap/scss/_variables.scss',
          'bootstrap/scss/_mixins.scss',
        ],
      },
    },
  ],
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.use('vue-svg-loader').loader('vue-svg-loader');
  },
};
